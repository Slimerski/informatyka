-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 03 Gru 2018, 07:09
-- Wersja serwera: 10.1.13-MariaDB
-- Wersja PHP: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `lingwistyka`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`) VALUES
(1, 'Fonetyka');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `comment` varchar(500) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `comments`
--

INSERT INTO `comments` (`comment_id`, `post_id`, `comment`, `user_id`, `date`) VALUES
(1, 1, 'Nigdy się nie poddawaj!', 2, '2018-12-02 06:26:10'),
(2, 1, 'awdawdawdawd', 1, '2018-12-03 02:13:46'),
(6, 9, 'ddwadawwdaawdawd', 1, '2018-12-03 06:48:38'),
(7, 11, 'awdawdawdwad', 9, '2018-12-03 07:06:21');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `images`
--

CREATE TABLE `images` (
  `img_id` int(11) NOT NULL,
  `img_name` varchar(100) NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `images`
--

INSERT INTO `images` (`img_id`, `img_name`, `post_id`) VALUES
(5, 'assets/img/posts/753eT5AvoR.png', 9),
(7, 'assets/img/posts/1_dudiw2.png', 11);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `posts`
--

CREATE TABLE `posts` (
  `post_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `content` text NOT NULL,
  `short_preview` varchar(250) NOT NULL,
  `post_author` int(11) NOT NULL,
  `thumbnail` varchar(60) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `posts`
--

INSERT INTO `posts` (`post_id`, `cat_id`, `title`, `content`, `short_preview`, `post_author`, `thumbnail`, `date`) VALUES
(9, 1, 'Najepszy świat', 'ugugłemwdaaaaaaaaawdawd', 'lubie całkiem', 1, 'assets/img/posts/1qtKvCEMBW.jpg', '2018-12-03'),
(11, 1, 'Czym jest fonem?', '<p>Strona skupia się na lingwistyce. Ma ona formę bloga / strony informacyjnej. Od zawsze interesowałem się językami i ich najważniejszymi częściami składowymi - słowami. \n      </p>\n\n      <p>Procesory wykLorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec aliquam leo. Integer velit dolor, viverra quis massa pulvinar, rutrum placerat velit. Vestibulum tristique ex nec fringilla luctus. Maecenas non vehicula velit. Etiam rhoncus enim sed nunc posuere hendrerit. Duis ac sagittis tortor. Vestibulum congue, quam eget sagittis molestie, lorem dolor consectetur nulla, et tempus tellus lorem condimentum diam. Aliquam erat volutpat. Donec quis fermentum tortor. In sed fermentum dolor.</p>\n\n      <p>Sed vulputate ante quis est mollis, ut iaculis mi laoreet. Phasellus velit sapien, dapibus eget massa vitae, sollicitudin dictum leo. Aliquam imperdiet dui justo, id posuere nulla placerat at. Phasellus et neque faucibus, luctus purus ac, bibendum elit. Quisque tempus condimentum nisl, vel elementum risus tempor ac. Fusce porta sit amet odio at venenatis. Nam nisl metus, bibendum quis velit eu, porttitor ultrices lacus. Sed eget velit turpis. Suspendisse in nunc lorem. Nunc cursus consequat enim, sed gravida sem sodales nec. Maecenas facilisis massa quis faucibus feugiat. Vestibulum vestibulum sollicitudin dui, vel mollis nunc tristique sit amet.</p>\n\n      <p>Phasellus luctus purus fringilla, pharetra nulla a, ultricies lorem. Praesent placerat magna eget neque euismod, in molestie diam pharetra. Morbi ut magna vel est fermentum imperdiet in nec eros. Quisque et nisi et tellus convallis elementum nec at ante. Nullam in risus nec orci placerat lacinia. Nunc bibendum nibh nec nisl sollicitudin, sit amet cursus urna posuere. Nullam eget lacinia neque. Phasellus a lacinia nibh. Vestibulum et dolor mollis, rutrum orci tincidunt, mattis ex. Vestibulum tincidunt odio vitae enim vestibulum pellentesque. Sed vehicula ligula lacus, ut convallis lectus tincidunt ac. Donec ut sem et quam maximus accumsan ac a massa. Sed consequat quam a nibh posuere, eu elementum risus congue. Sed ut venenatis nunc.</p>\n\n\n\n      <p>Na zewnątrz obudowy procesor wyposażony jest w wyprowadzenia, zazwyczaj w postaci nóżek lub, jak ma to miejsce w najnowszych konstrukcjach – w pola stykowe. Wyprowadzenia te umożliwiają procesorowi komunikację z innymi urządzeniami. W przypadku konstrukcji desktopowych czasami także umożliwiają one zamontowanie procesora w specjalnej podstawce. Wyprowadzenia te najczęściej są złocone. Walory estetyczne tego metalu oczywiście nie są tu istotne. Stosuje się go z powodu jego wyjątkowej odporności na utlenianie.</p>', 'Dowiedz się czym jest ten przeklęty fonem według różnych szkół lingwistyki.', 1, 'assets/img/posts/1_thumbnail.png', '2018-12-19');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `requests`
--

CREATE TABLE `requests` (
  `req_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `req_msg` varchar(500) NOT NULL,
  `req_date` date NOT NULL,
  `req_response` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `nick` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pass` varchar(160) NOT NULL,
  `act_code` int(8) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `type` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`user_id`, `nick`, `email`, `pass`, `act_code`, `active`, `type`) VALUES
(1, 'verseth', 'admin@gmail.com', '$2y$10$3e5lo.g6NlZ1.6IW1DgS6eP92LTcsZJPo0Szzfw/E7Zl.A3cPYeYK', 567453, 1, 1),
(2, 'emilia_gruchala', 'emilia.gruchala@gmail.com', '1242r2er21', 123456, 1, 0),
(3, 'dwawd', 'wadwad', 'awdawd', 12312, 1, 0),
(4, '$login', '$email', '$pass_hash', 2344, 1, 0),
(5, 'awdawdwda', 'gbgjnjhtyjy', '$2y$10$.a0dTHg0ozs9723IuMmJW.pt/H.6bTjqBQlS8b.3vnMOm02sSszOm', 23123, 1, 0),
(6, 'Juliusz', 'drewniak@gmail.com', '$2y$10$ahDCFXnxO65gv9q6B4qXnuzRhV/qmVg2Lx2Qcki8rgyPfujvqMdQK', 34343, 1, 0),
(7, 'wadwdaw', 'Przechuj11@gmail.com', '$2y$10$K5tkaHDugFEyOU9noCBNUum.mke6RsvdPe6Rh.1HsmH/kkiF0Vhs.', 539115, 1, 0),
(8, 'adminowy', 'adminowy@gmail.com', '$2y$10$qeZsz6eY.jyoBz4QfkDkxuraNmjC.Gf5ov49KeuPKrTp.TIghFA4m', 716174, 1, 0),
(9, 'adaman', 'adaman@gmail.com', '$2y$10$3e5lo.g6NlZ1.6IW1DgS6eP92LTcsZJPo0Szzfw/E7Zl.A3cPYeYK', 988173, 1, 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`img_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`req_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `images`
--
ALTER TABLE `images`
  MODIFY `img_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT dla tabeli `requests`
--
ALTER TABLE `requests`
  MODIFY `req_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
