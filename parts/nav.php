<?php
if (isset($_SESSION['logged_in'])) {
  $zalog = $_SESSION['nick'];
  $drop = '<a class="dropdown-item" href="php_scripts/logout.php">Wyloguj się</a>';
  if (isset($epic)) {
    $rejestracja = "Zarządzanie";
    $rejestracjaHref = "panel.php";
  }
  else {
    $rejestracja = "Rejestracja";
    $rejestracjaHref = "panel.php";
  }
}
else {
  $zalog = "Zaloguj się";
  $drop = '<form id="login" action="php_scripts/login.php" method="POST">
    <input class="form-control" type="text" name="login" placeholder="Login">
    <input class="form-control" type="password" name="pass" placeholder="Hasło">
    <button class="btn btn-light" type="submit">Zaloguj</button>
  </form>';
  $rejestracja = "Rejestracja";
  $rejestracjaHref = "rejestracja.php";
}
echo<<<HTML
  <header>
  <a href="index.php">/Lıŋˈgwıstıks/</a>
  </header>

  <nav>
   <div class="proper-nav">
     <ol>
       <li><a href="index.php"><span class="hide">Strona</span> Główna</a></li>


       <li><a href="$rejestracjaHref">$rejestracja</a></li>

       <li class="small-nav" style="border-right: 0;">
         <a style="color: #fff;" id="link-dropdown"  class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
     $zalog
     </a>
     <div class="dropdown-menu" aria-labelledby="link-dropdown">

       $drop

     </div>
       </li>
     </ol>
   </div>
   <div style="clear: both; display: none;"></div>

  </nav>
HTML;
?>
