<?php
  session_start();


  if (isset($_SESSION['logged_in']) && $_SESSION['account_type'] == 1) {
      $epic = true;
      
  }
  else $deleteComment = "";



 ?>

<!DOCTYPE html>
<html lang="pl">

<head>
  <?php require_once "parts/head.php"; ?>
</head>

<body>
  <div class="container-fluid" id="wrapper">

    <?php
    if(isset($_SESSION['login_error']))
    {
      if(!isset($_SESSION['login_error_success'])) $alert_color = ' alert-danger ';
      else{ $alert_color = ' alert-success '; unset($_SESSION['login_error_success']);}
      echo<<<HTML
      <div id="login-alert" class="alert-fade-index alert $alert_color alert-dismissible fade show" role="alert">
      $_SESSION[login_error]
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
HTML;

      unset($_SESSION['login_error']);
    }
      require_once "php_scripts/connect.php";
      require_once "parts/nav.php";
      echo<<<HTML
      <div class="row mx-auto">
HTML;


      if ($connection->connect_errno == 0) {
        $post_query = "SELECT posts.post_id, short_preview, cat_name, title, thumbnail, date
                  FROM posts
                  JOIN categories ON posts.cat_id = categories.cat_id
                  ORDER BY date DESC
                  LIMIT 4;";

        $post_result = $connection->query($post_query);

        if ($post_result->num_rows > 0) {

          $i = 0;
          while ($post_row = $post_result->fetch_assoc()) {

            $post_short_preview = $post_row['short_preview'];
            $post_cat_name = $post_row['cat_name'];
            $post_title = $post_row['title'];
            $post_id = $post_row['post_id'];
            $post_thumbnail = $post_row['thumbnail'];
            $post_month = date("n", strtotime($post_row['date'])); // liczbowa forma miesiąca od 1 do 12

            switch ($post_month) {
              case 1:
                $month = "Stycznia"; break;
              case 2:
                $month = "Lutego"; break;
              case 3:
                $month = "Marca"; break;
              case 4:
                $month = "Kwietnia"; break;
              case 5:
                $month = "Maja"; break;
              case 6:
                $month = "Czerwca"; break;
              case 7:
                $month = "Lipca"; break;
              case 8:
                $month = "Sierpnia"; break;
              case 9:
                $month = "Września"; break;
              case 10:
                $month = "Października"; break;
              case 11:
                $month = "Listopada"; break;
              case 12:
                $month = "Grudnia"; break;
            }

            $day = date("j", strtotime($post_row['date']));
            $year = date("Y", strtotime($post_row['date']));

            if ($i == 2 || $i == 3) $post_hide = "post-preview-hide";
            else $post_hide = "";

            if(isset($epic)) $deleteComment = "<a href=\"php_scripts/del_art.php?art=$post_id\"><i class=\"del-art fas fa-times-circle\"></i></a>";

            echo<<<HTML
            <div class="post-preview col-md-3 col-sm-6 p-0 $post_hide">

              <div class="post-thumbnail">
                <img src="$post_thumbnail" alt="post-thumbnail">
              </div>

              <div class="post-preview-content">
                <div class="category">$post_cat_name</div>
                <h2 class="mb-3"><a href="post.php?post=$post_id">$post_title </a>$deleteComment</h2>
                <p class="mb-4">$post_short_preview</p>
                <p><i class="fas fa-calendar-alt"></i> $day $month $year</p>
              </div>

            </div>
HTML;

            $i++;
          }
        }
      }
      else {
        echo "<h1 style=\"text-align: center\">Nastąpił błąd połączenia z bazą danych</h1>";
      }


    ?>
    </div>

    <article>

	<blockquote class="blockquote text-center">
  		<p class="mb-0">Jeśli starszawy, uznany naukowiec mówi, że coś jest możliwe, niemal z pewnością ma rację, lecz jeśli mówi, że to jest niemożliwe, najprawdopodobniej się myli.</p>
  		<footer class="blockquote-footer">Sir Arthur C. Clarke<cite title="Profiles of the Future"> w Profiles of the Future</cite></footer>
	</blockquote>
     <p>Strona skupia się na lingwistyce. Ma ona formę bloga / strony informacyjnej. Od zawsze interesowałem się językami i ich najważniejszymi częściami składowymi - słowami.
     </p>

     <p>Procesory wykLorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec aliquam leo. Integer velit dolor, viverra quis massa pulvinar, rutrum placerat velit. Vestibulum tristique ex nec fringilla luctus. Maecenas non vehicula velit. Etiam rhoncus enim sed nunc posuere hendrerit. Duis ac sagittis tortor. Vestibulum congue, quam eget sagittis molestie, lorem dolor consectetur nulla, et tempus tellus lorem condimentum diam. Aliquam erat volutpat. Donec quis fermentum tortor. In sed fermentum dolor.</p>

<p>Sed vulputate ante quis est mollis, ut iaculis mi laoreet. Phasellus velit sapien, dapibus eget massa vitae, sollicitudin dictum leo. Aliquam imperdiet dui justo, id posuere nulla placerat at. Phasellus et neque faucibus, luctus purus ac, bibendum elit. Quisque tempus condimentum nisl, vel elementum risus tempor ac. Fusce porta sit amet odio at venenatis. Nam nisl metus, bibendum quis velit eu, porttitor ultrices lacus. Sed eget velit turpis. Suspendisse in nunc lorem. Nunc cursus consequat enim, sed gravida sem sodales nec. Maecenas facilisis massa quis faucibus feugiat. Vestibulum vestibulum sollicitudin dui, vel mollis nunc tristique sit amet.</p>

<p>Phasellus luctus purus fringilla, pharetra nulla a, ultricies lorem. Praesent placerat magna eget neque euismod, in molestie diam pharetra. Morbi ut magna vel est fermentum imperdiet in nec eros. Quisque et nisi et tellus convallis elementum nec at ante. Nullam in risus nec orci placerat lacinia. Nunc bibendum nibh nec nisl sollicitudin, sit amet cursus urna posuere. Nullam eget lacinia neque. Phasellus a lacinia nibh. Vestibulum et dolor mollis, rutrum orci tincidunt, mattis ex. Vestibulum tincidunt odio vitae enim vestibulum pellentesque. Sed vehicula ligula lacus, ut convallis lectus tincidunt ac. Donec ut sem et quam maximus accumsan ac a massa. Sed consequat quam a nibh posuere, eu elementum risus congue. Sed ut venenatis nunc.</p>



	<p>Na zewnątrz obudowy procesor wyposażony jest w wyprowadzenia, zazwyczaj w postaci nóżek lub, jak ma to miejsce w najnowszych konstrukcjach – w pola stykowe. Wyprowadzenia te umożliwiają procesorowi komunikację z innymi urządzeniami. W przypadku konstrukcji desktopowych czasami także umożliwiają one zamontowanie procesora w specjalnej podstawce. Wyprowadzenia te najczęściej są złocone. Walory estetyczne tego metalu oczywiście nie są tu istotne. Stosuje się go z powodu jego wyjątkowej odporności na utlenianie.</p>


    </article>
    <footer>
      <?php require_once "parts/footer.php"; ?>
    </footer>
  </div>
<?php $connection->close(); ?>
</body>
</html>
