<?php
  session_start();

  if (!isset($_POST['post-name'])) {
    header("Location: ../index.php");
  }
  else {
    require_once "connect.php";
    require_once "functions.php";
    if ($connection->connect_errno != 0) {
      header("Location: ../index.php");
    }
    else {
      $allright = true;
      $user_id = $_SESSION['user_id'];
      $cat_id = $_POST['cat_id'];
      $post_name =$connection->real_escape_string( $_POST['post-name']);
      $post_desc = $connection->real_escape_string($_POST['post-desc']);
      $post_content = $connection->real_escape_string($_POST['post-content']);

      if (strlen($post_name) < 5) {
        $allright = false;
        $_SESSION['add_post'] = "Nazwa posta jest za krótka!(5 znaków)";
        $_SESSION['add_post_color'] = "alert-danger";
        header("Location: ../panel.php");
      }
      elseif (strlen($post_desc) < 5) {
        $allright = false;
        $_SESSION['add_post'] = "Opis posta jest za krótki!(5 znaków)";
        $_SESSION['add_post_color'] = "alert-danger";
        header("Location: ../panel.php");
      }
      elseif (strlen($post_content) < 20) {
        $allright = false;
        $_SESSION['add_post'] = "Treść posta jest za krótka!(20 znaków)";
        $_SESSION['add_post_color'] = "alert-danger";
        header("Location: ../panel.php");
      }
      elseif(!empty($_FILES['thumbnail']['name']) && !empty($_FILES['post-img']['name']))
      {
        $fplik = '../assets/img/posts/';
        if(!getimagesize($_FILES["thumbnail"]["tmp_name"]) && !getimagesize($_FILES['post-img']['name']))
        {
          $allright = false;
          $_SESSION['add_post'] = "Plik nie jest obrazem!";
          $_SESSION['add_post_color'] = "alert-danger";
          header("Location: ../panel.php");
        }
        elseif($_FILES["thumbnail"]["size"] > 10000000 || $_FILES["post-img"]["size"] > 10000000)
        {
          $allright = false;
          $_SESSION['add_post'] = "Obraz jest zbyt duży! (Limit to ~9MB)";
          $_SESSION['add_post_color'] = "alert-danger";
          header("Location: ../panel.php");
        }
        else
        {
          $randstring1 = random_string(10);  //LOSOWANIE STRINGA O OKRESLONEJ DLUGOSCI
          $randstring2 = random_string(10);

          $nazwa_pocz1 = basename($_FILES['thumbnail']['name']);
          $nazwa_pocz2 = basename($_FILES['post-img']['name']);
          $koniec_nazwy1 = strlen($nazwa_pocz1)-1;
          $koniec_nazwy2 = strlen($nazwa_pocz2)-1;
          $rozszerzenie1 = substr($nazwa_pocz1, strpos($nazwa_pocz1, ".") , $koniec_nazwy1);
          $rozszerzenie2 = substr($nazwa_pocz2, strpos($nazwa_pocz2, ".") , $koniec_nazwy2);
          $nplik1 = $randstring1.$rozszerzenie1; //NAZWA PLIKU - OR_randomowyciag.xxx
          $nplik2 = $randstring2.$rozszerzenie2;
          $celplik1 = $fplik.$nplik1;
          $celplik2 = $fplik.$nplik2;


          if(($rozszerzenie1!=".jpg" && $rozszerzenie1!=".jpeg" && $rozszerzenie1!=".bmp" && $rozszerzenie1!=".gif" && $rozszerzenie1!=".png")
            || ($rozszerzenie2!=".jpg" && $rozszerzenie2!=".jpeg" && $rozszerzenie2!=".bmp" && $rozszerzenie2!=".gif" && $rozszerzenie2!=".png"))
          {
            $_SESSION['add_post'] = "Zły format pliku. Akceptujemy tylko jpg, jpeg, bmp, gif i png.";
            $_SESSION['add_post_color'] = "alert-danger";
            header("Location: ../panel.php");
          }
          else{
            $allright = true;

            if(move_uploaded_file($_FILES['thumbnail']['tmp_name'], $celplik1))
            {
              $celplik1 = substr($celplik1, 3);
              $insert_post = $connection->query("INSERT INTO posts VALUES(NULL, $cat_id, '$post_name', '$post_content', '$post_desc', $user_id, '$celplik1', now())");
              $chapt_img_id_result = $connection->query("SELECT * FROM posts WHERE thumbnail = '$celplik1'");
              $chapt_img_id_row = $chapt_img_id_result->fetch_assoc();
              $chapt_img_id = $chapt_img_id_row['post_id'];
              $chapt_img_id_result->free_result();
              $newPostId = $chapt_img_id;

              if(move_uploaded_file($_FILES['post-img']['tmp_name'], $celplik2))
              {
                $celplik2 = substr($celplik2, 3);
                $insert_img = $connection->query("INSERT INTO images VALUES(NULL, '$celplik2', $newPostId);");
                $_SESSION['add_post'] = "Dodano posta!";
                $_SESSION['add_post_success'] = "alert-success";
                header("Location: ../panel.php");
              }
            }
          }
        }
      }
      else {
        $_SESSION['add_post'] = "Nie przesłano obrazu!";
        $_SESSION['add_post_color'] = "alert-danger";
        header("Location: ../panel.php");
      }



  }
}

 ?>
